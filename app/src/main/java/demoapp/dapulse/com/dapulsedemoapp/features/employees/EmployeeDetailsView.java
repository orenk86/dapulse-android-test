package demoapp.dapulse.com.dapulsedemoapp.features.employees;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import demoapp.dapulse.com.dapulsedemoapp.R;
import demoapp.dapulse.com.dapulsedemoapp.features.employees.models.Employee;

/**
 * Created by orenkosto on 10/2/17.
 */

public class EmployeeDetailsView extends RelativeLayout {

    @BindView(R.id.employee_phone_value)
    TextView employeePhone;

    @BindView(R.id.employee_email_value)
    TextView employeeEmail;

    public EmployeeDetailsView(Context context) {
        super(context);
        setup(context);
    }

    public EmployeeDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    private void setup(Context ctx) {
        inflateLayout(ctx);
        ButterKnife.bind(this);
    }

    public void inflateLayout(Context ctx) {
        LayoutInflater inflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.employee_details_view, this, true);
    }

    public void setEmployee(Employee employee) {
        employeeEmail.setText(employee.email);
        employeePhone.setText(employee.phone);
    }
}
