package demoapp.dapulse.com.dapulsedemoapp.features.employees;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import demoapp.dapulse.com.dapulsedemoapp.R;
import demoapp.dapulse.com.dapulsedemoapp.features.employees.base.BaseEmployeeActivity;
import demoapp.dapulse.com.dapulsedemoapp.features.employees.models.Employee;

public class EmployeeActivity extends BaseEmployeeActivity implements ListView.OnItemClickListener {

    public static final int TYPE_COMPANY = 0;
    public static final int TYPE_MANAGER= 1;
    public static final int TYPE_EMPLOYEE = 2;

    private int type = TYPE_COMPANY;
    private EmployeeListAdapter employeeListAdapter;
    private Employee currentEmployee;

    boolean showingDetails = false;

    @BindView(R.id.employee_name)
    TextView companyNameTv;

    @BindView(R.id.employee_details)
    EmployeeDetailsView employeeDetailsView;

    @BindView(R.id.employee_image)
    ImageView employeeImage;

    @BindView(R.id.employees_list)
    ListView employeeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        ButterKnife.bind(this);

        this.currentEmployee = Utils.getInstance().currentlyViewedEmployee;
        Intent intent = getIntent();
        if (intent.hasExtra("type")) {
            this.type = intent.getIntExtra("type", TYPE_COMPANY);
        }
        employeeDetailsView.setVisibility(View.GONE);

        this.employeeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showingDetails) {
                    Utils.getInstance().fadeIn(employeeList);
                    Utils.getInstance().fadeOut(employeeDetailsView);
                } else {
                    Utils.getInstance().fadeIn(employeeDetailsView);
                    Utils.getInstance().fadeOut(employeeList);
                }
                showingDetails = !showingDetails;
            }
        });
        this.employeeList.setOnItemClickListener(this);
        // make sure you call `presenter.loadCompany()` before any other call,
        // that's the method that fetch and parse the data.
        // if you'll try to call anything before `loadCompany` you won't have
        //  any data in the response.
        if (savedInstanceState == null && Utils.getInstance().employeeResponse == null) {
            presenter.loadCompany().subscribe(employeeResponse -> {
                Utils.getInstance().employeeResponse = employeeResponse;
                loadDataExample();
            });
        } else {
            loadDataExample();
        }
    }


    /************************************************************************
     * Here's some examples on how to get the data and subscribe to it.
     ***********************************************************************/
    private void loadDataExample() {
        switch (this.type) {
            case TYPE_COMPANY:
                presenter.getCompanyName().subscribe(company -> {
                    companyNameTv.setText(company);
                });
                presenter.getTopLevelManagement().subscribe(employees -> {
                    Log.d("EmployeeActivity", "got top level management data");
                    handleEmployees(employees);
                });
                break;
            case TYPE_MANAGER:
                companyNameTv.setText(currentEmployee.name);
                Picasso.with(this).load(currentEmployee.profilePic).into(employeeImage);
                employeeDetailsView.setEmployee(currentEmployee);
                presenter.getManagerEmployeesByManagerId(currentEmployee.id).subscribe(employees -> {
                    handleEmployees(employees);
                });
                break;
            case TYPE_EMPLOYEE:
                companyNameTv.setText(currentEmployee.name);
                Picasso.with(this).load(currentEmployee.profilePic).into(employeeImage);
                presenter.getEmployeeById(currentEmployee.id).subscribe(employee -> {
                    employeeDetailsView.setEmployee(employee);
                    Utils.getInstance().fadeIn(employeeDetailsView);
                    Utils.getInstance().fadeOut(employeeList);
                });
                break;
        }

//        presenter.getEmployeeById(16).subscribe(employee -> {
//            Log.d("EmployeeActivity", "got employee data");
//        });
//
//        presenter.getManagerEmployeesByManagerId(10).subscribe(employees -> {
//            Log.d("EmployeeActivity", "got manager employees data");
//        });
    }

    private void handleEmployees(List<Employee> employees) {
        employeeListAdapter = new EmployeeListAdapter(this, -1, employees);
        employeeList.setAdapter(employeeListAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("EmployeeActivity", "employee clicked");
        Employee employee = this.employeeListAdapter.getItem(position);
        Utils.getInstance().currentlyViewedEmployee = employee;
        Intent intent = new Intent(this, EmployeeActivity.class);
        intent.putExtra("type", employee.isManager != null && employee.isManager ? TYPE_MANAGER : TYPE_EMPLOYEE);
        startActivity(intent);
    }
}
