package demoapp.dapulse.com.dapulsedemoapp.features.employees;

import android.content.Context;
import android.view.View;

import demoapp.dapulse.com.dapulsedemoapp.features.employees.models.Employee;
import demoapp.dapulse.com.dapulsedemoapp.features.employees.models.EmployeeResponse;

/**
 * Created by orenkosto on 10/2/17.
 */
public class Utils {

    private static Utils instance;
    private final Context context;

    EmployeeResponse employeeResponse;

    Employee currentlyViewedEmployee;

    public Utils(Context ctx) {
        this.context = ctx;
    }

    public static void init(Context context) {
        if (instance == null) {
            context = context.getApplicationContext();
            instance = new Utils(context);
        }
    }

    public static Utils getInstance() {
        return instance;
    }

    public Context getContext() {
        return context;
    }

    public void fadeIn(View view) {
        if (view.getVisibility() != View.VISIBLE) {
            view.setAlpha(0);
            view.setVisibility(View.VISIBLE);
        }
        view.animate().alpha(1).setDuration(150);
    }

    public void fadeOut(View view) {
        view.animate().alpha(0).setDuration(150);
    }
}
