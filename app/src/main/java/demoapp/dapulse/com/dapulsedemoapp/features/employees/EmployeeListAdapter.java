package demoapp.dapulse.com.dapulsedemoapp.features.employees;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import demoapp.dapulse.com.dapulsedemoapp.R;
import demoapp.dapulse.com.dapulsedemoapp.features.employees.models.Employee;

/**
 * Created by orenkosto on 10/2/17.
 */

public class EmployeeListAdapter extends ArrayAdapter<Employee> {

    private final Context context;
    private final List<Employee> employees;

    public EmployeeListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Employee> employees) {
        super(context, resource, employees);
        this.context = context;
        this.employees = employees;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout rowView = (RelativeLayout) inflater.inflate(R.layout.employees_list_item_view, parent, false);
        Employee employee = this.employees.get(position);

        ImageView employeeItemImage = (ImageView) rowView.findViewById(R.id.employee_item_image);
        TextView employeeItemName = (TextView) rowView.findViewById(R.id.employee_item_name);

        Picasso.with(this.context).load(employee.profilePic).into(employeeItemImage);
        employeeItemName.setText(employee.name);

        return rowView;
    }

    @Nullable
    @Override
    public Employee getItem(int position) {
        return this.employees.get(position);
    }
}
